const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function () {
  return {
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            // MiniCssExtractPlugin.loader,
            'style-loader',
            { loader: 'css-loader', options: { sourceMap: true, } },
            'postcss-loader',
            { loader: 'sass-loader', options: { sourceMap: true, } },
          ]
        },
      ]
    },
    // plugins: [
    //   new MiniCssExtractPlugin({ filename: 'styles/styles.css' })
    // ],
  };
};

module.exports = function(isDevMode) {
  return {
    entry: isDevMode ? 'index.dev.js' : 'index.prod.js',
  };
};

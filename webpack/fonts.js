module.exports = function () {
  return {
    module: {
      rules: [
        {
          test: /\.(ttf|eot|woof|woof2)$/i,
          exclude: /\/node_modules\//,
          use: {
            loader: 'file-loader',
            options: {
              esModule: false,
              outputPath: 'assets/fonts',
              name: '[name].[ext]',
              limit: 1024,
              publicPath: (url, resourcePath, context) => `../assets/fonts/${url}`,
            },
          },
        },
      ]
    },
  };
};

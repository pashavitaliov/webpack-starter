import { all, take, select } from 'redux-saga/effects';
import dogSaga from './containers/dog/sagas';

function* watchAndLog() {
  while (true) {
    const action = yield take('*');
    const state = yield select();

    console.log('action', action);
    console.log('state after', state);
  }
}

export default function* rootSaga() {
  yield all([
    dogSaga(),
    // watchAndLog()
  ]);
}
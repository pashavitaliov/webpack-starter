import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware, ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';

import './styles/main.scss';

import AppContainer from 'containers/appContainer';

import reducer from './rootReducer';
import sagas from './rootSaga';

const history = createBrowserHistory();
const router = routerMiddleware(history);

const saga = createSagaMiddleware();

const store = createStore(reducer(history), composeWithDevTools(applyMiddleware(router, saga)));
saga.run(sagas);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AppContainer />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);

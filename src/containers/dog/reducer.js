export const TYPES = {
  REQUESTED_DOG: 'REQUESTED_DOG',
  REQUESTED_DOG_SUCCEEDED: 'REQUESTED_DOG_SUCCEEDED',
  REQUESTED_DOG_FAILED: 'REQUESTED_DOG_FAILED',
};

const initialState = {
  url: '',
  loading: false,
  error: false
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.REQUESTED_DOG:
      return {
        ...state,
        loading: true
      };
    case TYPES.REQUESTED_DOG_SUCCEEDED:
      return {
        ...state,
        url: action.url,
        loading: false,
      };
    case TYPES.REQUESTED_DOG_FAILED:
      return {
        ...state,
        error: true
      };
    default:
      return state;
  }
}

export const ACTIONS = {
  requestDog: () => ({ type: TYPES.REQUESTED_DOG }),
  requestDogSucceeded: data => ({ type: TYPES.REQUESTED_DOG_SUCCEEDED, url: data.message }),
  requestDogFailed: () => ({ type: TYPES.REQUESTED_DOG_FAILED })
};

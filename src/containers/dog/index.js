import React, { Component } from 'react';
import { connect } from 'react-redux';

import { ACTIONS } from './reducer';
import { dogSelector } from './selector';

import './styles.scss';

class Dog extends Component {
  // fetchDog = () => {
  //   const { requestDog, requestDogSucceeded, requestDogFailed } = this.props;
  //   requestDog();
  //   return fetch('https://dog.ceo/api/breeds/image/random')
  //     .then(res => res.json())
  //     .then((result) => {
  //       requestDogSucceeded(result);
  //       return result;
  //     })
  //     .then(result => (console.log(result)))
  //     .catch(requestDogFailed());
  // };

  render() {
    console.log(this.props);
    const { dogReducer: { url, loading }, requestDog } = this.props;

    return (
      <div className="dog">
        <button type="button" onClick={() => requestDog()}>
          {loading ? 'LoadingF...' : 'LoadingF'}
        </button>
        <div className="img-container"><img src={url} alt="" /></div>
      </div>
    );
  }
}

export default connect(
  state => {
    console.log(state);
    return {
      dogReducer: state.dogReducer,
      dogReducer2: dogSelector(state),
    };
  },
  { ...ACTIONS }
)(Dog);

// dispatch => ({
//   requestDog: () => dispatch(ACTIONS.requestDog()),
//   requestDogSucceeded: (data) => dispatch(ACTIONS.requestDogSucceeded(data)),
//   requestDogFailed: () => dispatch(ACTIONS.requestDogFailed())
// })

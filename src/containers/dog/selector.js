import { createSelector } from 'reselect';

const getDogState = (state) => state.dogReducer;

export const dogSelector = createSelector(
  getDogState,
  (dogState) => {
    console.warn(dogState);
    return dogState;
  }
);

import React, { Component } from 'react';
import { hot } from 'react-hot-loader/root';
import App from 'containers/app';

class AppContainer extends Component {
  render() {
    return (
      <App />
    );
  }
}

export default hot(AppContainer);

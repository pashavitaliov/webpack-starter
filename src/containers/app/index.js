import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import Comp1 from 'components/comp1';
import Comp2 from 'components/comp2';
import Dog from 'containers/dog';


class App extends Component {
  render() {
    console.log(12)
    return (
      <div>
        <Link to="/">/</Link>
        <Link to="comp1">comp1</Link>
        <Link to="comp2">comp2</Link>
        <Link to="dog">dog</Link>

        <Switch>
          <Route exact path="/" render={() => <h1>123qwe</h1>} />
          <Route path="/comp1" component={Comp1} />
          <Route path="/comp2" component={Comp2} />
          <Route path="/dog" component={Dog} />

        </Switch>

        <Dog />
      </div>
    );
  }
}

export default App;

export const getToken = () => {
  return JSON.parse(localStorage.getItem('sessionToken'));
};

export const setToken = (token) => {
  if (typeof token !== 'string') {
    throw new Error('token invalid');
  }

  localStorage.setItem('sessionToken', JSON.stringify(token));
}

export const deleteToke = () => {
  localStorage.removeItem('sessionToken');
}

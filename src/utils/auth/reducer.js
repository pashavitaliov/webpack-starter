export const TYPES = {
  AUTH_FETCH: 'AUTH_FETCH',
  AUTH_SUCCEEDED: 'AUTH_SUCCEEDED',
  AUTH_FAILED: 'AUTH_FAILED',
  AUTH_SET_TOKEN: 'AUTH_SET_TOKEN',
  AUTH_DETELE_TOKEN: 'AUTH_DETELE_TOKEN'
};

const initialState = {
  token: '',
  isAuth: false,
  loading: false,
  error: ''
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.AUTH_FETCH:
      return {
        ...state,
        loading: true
      };
    case TYPES.AUTH_SUCCEEDED:
      return {
        ...state,
        token: action.token,
        loading: false,
        error: '',
      };
    case TYPES.AUTH_FAILED:
      return {
        ...state,
        error: true,
        token: '',
        isAuth: false,
      };
    case TYPES.AUTH_SET_TOKEN:
      return {
        ...state,
        token: action.token
      };
    case TYPES.AUTH_DETELE_TOKEN:
      return {
        ...state,
        token: ''
      };
    default:
      return state;
  }
}

export const ACTIONS = {
  requestAuth: () => ({ type: TYPES.AUTH_FETCH }),
  requestAuthSucceeded: data => ({ type: TYPES.AUTH_SUCCEEDED, url: data.message }),
  requestAuthFailed: () => ({ type: TYPES.AUTH_FAILED }),
  setAuthToken: (data) => ({ type: TYPES.AUTH_SET_TOKEN, token: data.token }),
  deleteAuthToken: () => ({ type: TYPES.AUTH_DETELE_TOKEN_TOKEN })
};

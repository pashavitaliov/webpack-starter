import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as authReducer } from 'utils/auth/reducer';
import { reducer as dogReducer } from 'containers/dog/reducer';

export default history => combineReducers({
  router: connectRouter(history),
  authReducer,
  dogReducer,
});
